package com.example.excel.com;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Cipher;
import java.io.InputStream;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

@RestController
public class EncryptionController {

    @GetMapping("/encrypt")
    public String encryptData(@RequestParam String data) {
        try {
            // Đọc tệp .cer từ thư mục resources
            Resource resource = new ClassPathResource("self-signed.cer");
            InputStream cerInputStream = resource.getInputStream();
            PublicKey publicKey = getPublicKeyFromCertificate(cerInputStream);

            // Mã hóa dữ liệu
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encryptedData = cipher.doFinal(data.getBytes());
            // Chuyển đổi kết quả mã hóa thành dạng Base64
            String base64EncryptedData = Base64.getEncoder().encodeToString(encryptedData);

            return base64EncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }

    }

    private PublicKey getPublicKeyFromCertificate(InputStream cerInputStream) throws Exception {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        X509Certificate x509Certificate = (X509Certificate) certificateFactory.generateCertificate(cerInputStream);
        return x509Certificate.getPublicKey();
    }

}

